﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TurnerSoftware.Sitemap;
using UkadGroupApp.Models;

namespace UkadGroupApp.Controllers
{
    public class TestURLController : Controller
    {
        public ActionResult Test(string url)
        {
            List<RequestData> model = new List<RequestData>();
            SitemapQuery sitemapQuery = new SitemapQuery();
            var sitemaps = sitemapQuery.RetrieveSitemapsForDomain(new Uri(url).Host);
            var _client = new MongoClient("mongodb://localhost:27017");
            var _database = _client.GetDatabase("test");
            var data = _database.GetCollection<RequestData>("RequestDataCollection");

            foreach (var sitemap in sitemaps)
            {
                foreach(var sitemapUrl in sitemap.Urls)
                {
                    model.Add(new RequestData(sitemapUrl.Location.ToString(), GetRequestTimespan(sitemapUrl.Location)));
                }
            }

            data.InsertMany(model);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataForDomainName(string domainName)
        {
            var _client = new MongoClient("mongodb://localhost:27017");
            var _database = _client.GetDatabase("test");
            var data = _database.GetCollection<RequestData>("RequestDataCollection").AsQueryable().Where(x => x.domainName == domainName).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private TimeSpan GetRequestTimespan(Uri url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            Stopwatch sw = Stopwatch.StartNew();
            var response = request.GetResponse();
            sw.Stop();
            return sw.Elapsed;
        }
    }
}
