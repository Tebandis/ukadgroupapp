﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UkadGroupApp.Models;

namespace UkadGroupApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var _client = new MongoClient("mongodb://localhost:27017");
            var _database = _client.GetDatabase("test");
            var data = _database.GetCollection<RequestData>("RequestDataCollection").AsQueryable().Select(x => x.domainName).Distinct().ToList();

            return View(data);
        }
    }
}