﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TurnerSoftware.Sitemap;

namespace UkadGroupApp.Models
{
    public class RequestData
    {
        public ObjectId _id { get; set; }
        public TimeSpan RequestTime { get; set; }
        public string RequestUrl { get; set; }
        public string domainName { get; set; }

        public RequestData(string requestUrl, TimeSpan requestTime)
        {
            RequestUrl = requestUrl;
            RequestTime = requestTime;
            domainName = new Uri(requestUrl).Host;
        }

        public RequestData()
        {

        }
    }
}